# -*- coding: utf-8 -*-
import os
import sys
import io
import datetime
#sys.stdout.io.TextIOWrapper(sys.stdout.buffer,encoding = 'utf-8')

class process():
    def __init__(self,data):
        #是否流量超套餐
        self.is_gprs_over = data["is_gprs_over"]
        #是否语音超套餐
        self.is_gsm_over = data["is_gsm_over"]
        #当月总使用流量
        self.gprs_all = data["gprs_all"]
        #当月总使用语音
        self.gsm_all = data["gsm_all"]
        #当月剩余流量
        self.gprs_residua = data["gprs_residua"]
        #当月剩余语音
        self.gsm_residua = data["gsm_residua"]
        #当月日均使用流量
        self.avg_day_gprs = data["avg_day_gprs"]
        #当月日均使用语音
        self.avg_day_gsm = data["avg_day_gsm"]
        #当前距月末几天
        self.suf_cur_day_count = data["suf_current_day_count"]
        #近三个月语音超套次数
        self.avg_three_month_gsm_count = data["avg_three_month_gsm_count"]
        #近三个月流量超套次数
        self.avg_three_month_gprs_count = data["avg_three_month_gprs_count"]
        #近三个月平均套外流量费
        self.avg_three_month_gprs_fee = data["avg_three_month_gprs_fee"]
        #近三个月平均套外语音费
        self.avg_three_month_gsm_fee = data["avg_three_month_gsm_fee"]
        #近三个月平均套外语音使用量
        self.avg_three_month_gsm = data["avg_three_month_gsm"]
        #近三个月平均套外流量使用量
        self.avg_three_month_gprs = data["avg_three_month_gprs"]
        #账务月套外上网费
        self.tcw_gprs_fee1 = data["tcw_gprs_fee1"]
        #账务月套外语音费
        self.tcw_gsm_fee1 = data["tcw_gsm_fee1"]
        #账外月套内总使用流量
        self.gprs_main_all1 = data["gprs_main_all1"]
        #账务月套外上网使用量
        self.tcw_gprs1 = data["tcw_gprs1"]
        #账务月总使用语音量
        self.gsm_zj1 = data["gsm_zj1"]
        #账务月套外总使用语音量
        self.tcw_gsm1 = data["tcw_gsm1"]
        #账务月减一套外上网费
        self.tcw_gprs_fee2 = data["tcw_gprs_fee2"]
        #账务月减一套外语音费
        self.tcw_gsm_fee2 = data["tcw_gsm_fee2"]
        #账务月减一套外上网流量
        self.tcw_gprs2 = data["tcw_gprs2"]
        #账务月减一套外语音使用量
        self.tcw_gsm2 = data["tcw_gsm2"]
        #账务月减二套外上网费
        self.tcw_gprs_fee3 = data["tcw_gprs_fee3"]
        #账务月减二套外语音费
        self.tcw_gsm_fee3 = data["tcw_gsm_fee3"]
        #账务月减二套外上网流量
        self.tcw_gprs3 = data["tcw_gprs3"]
        #账务月减二套外语音使用量
        self.tcw_gsm3 = data["tcw_gsm3"]
        #是否有活动到期
        self.over_activity = data["is_over_activity"]
        #上个月失效活动个数
        self.pre_day_count = data["pre_current_day_count"]
        #本月失效活动个数
        self.current_month_count = data["current_month_count"]
        #次日失效活动个数
        self.next_day_count = data["next_day_count"]
        #次月失效活动个数
        self.next_month_count = data["next_month_count"]
        #主套餐资费
        self.now_prod_value = data["now_prod_value"]
        #主套餐包含流量
        self.now_prod_gprs = data["now_prod_gprs"]
        #主套餐包含语音
        self.now_prod_gsm = data["now_prod_gsm"]
        #当月总使用流量
        self.gprs_all = data["gprs_all"]
        #当月总使用语音
        self.gsm_all = data["gsm_all"]
        #self.data_content = data["data_content"]
        self.productDesc = data["productDesc"]
        #坐席勾选的场景标签
        self.label_list = {"cur_month_gsm_over":1,"cur_month_gprs_over":1,"two_last_month_gsm_over":1,"two_last_month_gprs_over":1,"cur_month_gsm_will_over":1,"cur_month_gprs_will_over":1,"three_month_gsm_over":1,"three_month_gprs_over":1,"last_month_gsm_over":1,"last_month_gprs_over":1,"over_activity":1,"pre_day_num":1,"current_month_num":1,"next_day_num":1,"next_month_num":1}
    #当月语音是否超套
    def is_cur_month_gprs_over(self):
        if self.is_gprs_over >= 1:
            return True
        return False
    #当月流量是否超套
    def is_cur_month_gsm_over(self):
        if self.is_gsm_over >= 1 :
            return True
        return False
    #账务月流量超套
    def is_last_month_gprs_over(self):
        if self.tcw_gprs_fee1 > 0 and self.tcw_gprs1 > 0:
            return True
        return False
    #账务月语音超套
    def is_last_month_gsm_over(self): 
        if self.tcw_gsm_fee1 > 0 or self.tcw_gsm1 > 0:
            return True
        return False
    #近三个月流量均超套
    def is_three_month_gprs_over(self):
        if self.avg_three_month_gprs_count >= 3 :
            return True
        return False
    #近三个月语音均超套
    def is_three_month_gsm_over(self):
        if self.avg_three_month_gsm_count >= 3 :
            return True
        return False
    #当月流量预将超套
    def is_cur_month_gprs_will_over(self):
        if self.now_prod_gprs <= 0 or self.suf_cur_day_count <= 0:
            return False
        if self.gprs_residua/self.now_prod_gprs < 0.3 and self.gprs_residua / self.suf_cur_day_count < self.avg_day_gprs and self.gprs_residua / self.suf_cur_day_count < 0.01:
            return True
        return False
    #当月语音预将超套
    def is_cur_month_gsm_will_over(self):
        if self.now_prod_gprs <= 0 or self.suf_cur_day_count <= 0:
            return False
        if self.gsm_residua/self.now_prod_gsm < 0.15 and self.gsm_residua / self.suf_cur_day_count < self.avg_day_gsm and self.gsm_residua/self.suf_cur_day_count < 2 :
            return True
        return False
    #上个账务月流量超套
    def is_two_last_month_gprs_over(self):
        if self.tcw_gprs_fee2 > 0 or self.tcw_gprs2 > 0:
            return True
        return False
    
    #上个账务月语音超套
    def is_two_last_month_gsm_over(self): 
        if self.tcw_gsm_fee2 > 0 or self.tcw_gsm2 > 0:
            return True
        return False
    #上上个帐务月流量超套
    def is_three_last_month_gprs_over(self):
        if self.tcw_gprs_fee3 > 0 or self.tcw_gprs3 > 0 :
            return True
        return False
    #上上个账务月语音超套
    def is_three_last_month_gsm_over(self):
        if self.tcw_gsm_fee3 > 0 or self.tcw_gsm3 > 0:
            return True
        return False
    
    #活动到期
    def is_over_activity(self):
        if self.pre_day_count > 0 or self.current_month_count > 0 or self.next_day_count > 0 or self.next_month_count > 0:
            return 1
        return 0
    #上月有办理的活动已到期
    def is_pre_day_num(self):
        if  self.pre_day_count > 0:
            return self.pre_day_count
        return 0
    #本月有办理的活动将到期
    def is_current_month_num(self):
        if  self.current_month_count > 0:
            return self.current_month_count
        return 0
    #次日有办理的活动将到期
    def is_next_day_num(self):
        return self.next_day_count
    #次月有办理的活动将到期
    def is_next_month_num(self):
        return self.next_month_count
    #预测用户场景标签
    def pre_label(self):
        label_dict = {"cur_month_gsm_over":0,"cur_month_gprs_over":0,"two_last_month_gsm_over":0,
                      "two_last_month_gprs_over":0,"cur_month_gsm_will_over":0,"cur_month_gprs_will_over":0,
                    "three_month_gsm_over":0,"three_month_gprs_over":0,"last_month_gsm_over":0,
                    "last_month_gprs_over":0,"over_activity":0,"pre_day_num":0,"current_month_num":0,
                    "next_day_num":0,"next_month_num":0,
                    "three_last_month_gsm_over":0,
                    "three_last_month_gprs_over":0
                    
                    }
        if self.is_cur_month_gsm_over():
            label_dict["cur_month_gsm_over"] = 1
        else:
            if self.is_cur_month_gsm_will_over():
                label_dict["cur_month_gsm_will_over"] = 1
        if self.is_cur_month_gprs_over():
            label_dict["cur_month_gprs_over"] = 1
        else:
            if self.is_cur_month_gprs_will_over():
                label_dict["cur_month_gprs_will_over"] = 1
        if self.is_three_month_gprs_over():
            label_dict["three_month_gprs_over"] = 1
        else:
            if self.is_last_month_gprs_over():
                label_dict["last_month_gprs_over"] = 1 
            if self.is_two_last_month_gprs_over():
                label_dict["two_last_month_gprs_over"] = 1
            if self.is_three_last_month_gprs_over():
                label_dict["three_last_month_gprs_over"] = 1

        if self.is_three_month_gsm_over():
            label_dict["three_month_gsm_over"] = 1
        else:
            if self.is_last_month_gsm_over():
                label_dict["last_month_gsm_over"] = 1 
            if self.is_two_last_month_gsm_over():
                label_dict["two_last_month_gsm_over"] = 1 
            if self.is_three_last_month_gsm_over():
                label_dict["three_last_month_gsm_over"] = 1
        label_dict["over_activity"] = self.is_over_activity()
        label_dict["pre_day_num"] = self.is_pre_day_num()
        label_dict["current_month_num"] = self.is_current_month_num()
        label_dict["next_day_num"] = self.is_next_day_num()
        label_dict["next_month_num"] = self.is_next_month_num()
        return label_dict
    def activates(self,label_dict):
        
        first_str = "且办理的活动于"
        temp = ""
        if label_dict["pre_day_num"] >= 1:
            temp+="上月,"
        if label_dict["current_month_num"] >= 1:
            temp+="本月,"
        if temp != "":
            return first_str + temp[:-1] + "已到期，"
        if label_dict["next_day_num"]  >= 1:
            temp+="次日,"
        if label_dict["next_month_num"] >= 1:
            temp+="次月,"
        if temp != "":
            return first_str + temp[:-1] + "已到期，"

        return temp
    #产生切入语及场景话术
    def get_month(self):
        month = datetime.datetime.now().month
        month1 = str(month -1) + "月"
        if month -1 <= 0:
            month1 = "去年" + str(month -1 + 12) + "月"
        month2 = str(month -2) + "月"
        if month -2 <= 0:
            month2 = "去年" + str(month -2 + 12) + "月"
        month3 = str(month -3) + "月"
        if month -3 <= 0:
            month3 = "去年" + str(month -3 + 12) + "月"
        return month1,month2,month3
    def product_chinese(self,label_dict):
        
        first_chi = []
        scene_chi = []
        flag = {}
        first_chi_str = ""
        month1,month2,month3 = self.get_month()
        if label_dict["cur_month_gsm_over"] >= 1 and label_dict["cur_month_gprs_over"] >= 1:
            over_gprs = self.gprs_all - self.now_prod_gprs
            over_gsm = self.gsm_all -self.now_prod_gsm
            over_gprs_fee = 0
            over_gsm_fee = 0
            tem = "当月"
            temp = "当月语音流量均已超套；本月超出：" + str(over_gprs) + "G；产生额外流量费" + str(over_gprs_fee) + "元；本月超出：" + str(over_gsm) + "分钟；产生额外语音费:" + str(over_gsm_fee) + "元；"
            first_chi.append(tem)
            scene_chi.append(temp)
            flag = {"y":"语音","L":"流量"}
        elif label_dict["cur_month_gsm_over"] >= 1 :
            over_gprs = self.gprs_all - self.now_prod_gprs
            over_gsm = self.gsm_all -self.now_prod_gsm
            over_gprs_fee = 0
            over_gsm_fee = 0
            tem = "当月" 
            temp = "当月语音已超套,本月超出：" + str(over_gsm) + "分钟；产生额外语音费:" + str(over_gsm_fee) + "元；"  
            if tem not in first_chi:
                first_chi.append(tem)
            if "y" not in flag:
                flag["y"] = "语音"
            scene_chi.append(temp)   
        elif label_dict["cur_month_gprs_over"] >= 1 :
            over_gprs = self.gprs_all - self.now_prod_gprs
            over_gsm = self.gsm_all -self.now_prod_gsm
            over_gprs_fee = 0
            over_gsm_fee = 0
            tem = "当月"
            temp = "当月流量已超套,本月超出：" + str(over_gsm) + "分钟；产生额外流量费:" + str(over_gsm_fee) + "元；"   
            if tem not in first_chi:
                first_chi.append(tem)
            if "L" not in flag:
                flag["L"] = "流量"
            scene_chi.append(temp)
        if label_dict["three_month_gsm_over"] >= 1 and label_dict["three_month_gprs_over"] >= 1:
            over_gprs = self.avg_three_month_gprs*3/1024
            over_gsm = self.avg_three_month_gsm*3
            over_gprs_fee = self.avg_three_month_gprs_fee*3
            over_gsm_fee = self.avg_three_month_gsm_fee*3

            tem = "近三个月"
            temp = "近三个月语音流量均已超套;超出:"+str(over_gprs)+ "G；产生额外流量费:"+str(over_gprs_fee)+"元；超出：" + str(over_gsm)+"分钟；产生额外语音费"+str(over_gsm_fee)+"元；"
            if tem not in first_chi:
                first_chi.append(tem)
            flag = {"y":"语音","L":"流量"}
            scene_chi.append(temp)
        elif label_dict["three_month_gsm_over"] >= 1:
            over_gprs = self.avg_three_month_gprs*3/1024
            over_gsm = self.avg_three_month_gsm*3
            over_gprs_fee = self.avg_three_month_gprs_fee*3
            over_gsm_fee = self.avg_three_month_gsm_fee*3

            tem = "近三个月"
            if tem not in first_chi:
                first_chi.append(tem)
            if "y" not in flag:
                flag["y"] = "语音"
            temp = "近三个月语音已超套;超出:" + str(over_gsm)+"分钟；产生额外语音费:"+str(over_gsm_fee)+"元；"
            scene_chi.append(temp)
        elif label_dict["three_month_gprs_over"] >= 1:
            over_gprs = self.avg_three_month_gprs*3/1024
            over_gsm = self.avg_three_month_gsm*3
            over_gprs_fee = self.avg_three_month_gprs_fee*3
            over_gsm_fee = self.avg_three_month_gsm_fee*3

            tem = "近三个月"
            temp = "近三个月流量已超套；超出："+ str(over_gprs)+"G；产生额外流量费"+str(over_gprs_fee)+"元；"
            if tem not in first_chi:
                first_chi.append(tem)
            if "L" not in flag:
                flag["L"] = "流量"
            scene_chi.append(temp)
        if label_dict["last_month_gsm_over"] >= 1 and label_dict["last_month_gprs_over"] >= 1:
            
            over_gprs = self.tcw_gprs1/1024
            over_gsm = self.tcw_gsm1
            over_gprs_fee = self.tcw_gprs_fee1
            over_gsm_fee = self.tcw_gsm_fee1
            tem = month1
            temp = "账务月语音流量均已超套；超出："+str(over_gprs)+ "G；产生额外流量费"+str(over_gprs_fee)+"元；超出：" + str(over_gsm)+"分钟；产生额外语音费"+str(over_gsm_fee)+"元；"
            if tem not in first_chi:
                first_chi.append(tem)
            flag = {"y":"语音","L":"流量"}
            scene_chi.append(temp)
        elif label_dict["last_month_gsm_over"] >= 1 :
            over_gprs = self.tcw_gprs1/1024
            over_gsm = self.tcw_gsm1
            over_gprs_fee = self.tcw_gprs_fee1
            over_gsm_fee = self.tcw_gsm_fee1

            tem = month1
            temp = "账务月语音已超套；超出："+ str(over_gsm)+"分钟；产生额外语音费"+str(over_gsm_fee)+"元；"
            if tem not in first_chi:
                first_chi.append(tem)
            if "y" not in flag:
                flag["y"] = "语音"
            scene_chi.append(temp)
        elif label_dict["last_month_gprs_over"] >= 1:
            over_gprs = self.tcw_gprs1/1024
            over_gsm = self.tcw_gsm1
            over_gprs_fee = self.tcw_gprs_fee1
            over_gsm_fee = self.tcw_gsm_fee1
            tem = month1
            temp = "账务月流量已超套；超出："+ str(over_gprs)+"G；产生额外流量费"+str(over_gprs_fee)+"元；"
            if tem not in first_chi:
                first_chi.append(tem)
            if "L" not in flag:
                flag["L"] = "流量"
            scene_chi.append(temp)
        if label_dict["two_last_month_gsm_over"] >= 1 and label_dict["two_last_month_gprs_over"] >= 1:
            over_gprs = self.tcw_gprs2/1024
            over_gsm = self.tcw_gsm2
            over_gprs_fee = self.tcw_gprs_fee2
            over_gsm_fee = self.tcw_gsm_fee2
            tem = month2
            temp = month2 + "语音流量均已超套；超出："+str(over_gprs)+ "G；产生额外流量费"+str(over_gprs_fee)+"元；超出：" + str(over_gsm)+"分钟；产生额外语音费"+str(over_gsm_fee)+"元；"
            if tem not in first_chi:
                first_chi.append(tem)
            flag = {"y":"语音","L":"流量"}
            scene_chi.append(temp)
        elif label_dict["two_last_month_gsm_over"] >= 1 :
            over_gprs = self.tcw_gprs2
            over_gsm = self.tcw_gsm2
            over_gprs_fee = self.tcw_gprs_fee2
            over_gsm_fee = self.tcw_gsm_fee2

            tem = month2 
            temp = month2 + "语音已超套；超出："+ str(over_gsm)+"分钟；产生额外语音费"+str(over_gsm_fee)+"元；"
            if tem not in first_chi:
                first_chi.append(tem)
            if "y" not in flag:
                flag["y"] = "语音"
            scene_chi.append(temp)
        elif label_dict["two_last_month_gprs_over"] >= 1:
            over_gprs = self.tcw_gprs2
            over_gsm = self.tcw_gsm2
            over_gprs_fee = self.tcw_gprs_fee2
            over_gsm_fee = self.tcw_gsm_fee2
            tem = month2 
            temp = month2 + "流量已超套；超出："+ str(over_gprs)+"G；产生额外流量费"+str(over_gprs_fee)+"元；"
            if tem not in first_chi:
                first_chi.append(tem)
            if "L" not in flag:
                flag["L"] = "流量"
            scene_chi.append(temp)
        
        if label_dict["three_last_month_gsm_over"] >= 1 and label_dict["three_last_month_gprs_over"] >= 1:
            over_gprs = self.tcw_gprs3/1024
            over_gsm = self.tcw_gsm3
            over_gprs_fee = self.tcw_gprs_fee3
            over_gsm_fee = self.tcw_gsm_fee3
            tem = month3
            temp = month3 + "语音流量均已超套；超出："+str(over_gprs)+ "G；产生额外流量费"+str(over_gprs_fee)+"元；超出：" + str(over_gsm)+"分钟；产生额外语音费"+str(over_gsm_fee)+"元；"
            if tem not in first_chi and "近三个月" not in first_chi:
                first_chi.append(tem)
            flag = {"y":"语音","L":"流量"}
            scene_chi.append(temp)
        elif label_dict["three_last_month_gsm_over"] >= 1 :
            over_gprs = self.tcw_gprs3
            over_gsm = self.tcw_gsm3
            over_gprs_fee = self.tcw_gprs_fee3
            over_gsm_fee = self.tcw_gsm_fee3
            tem = month3
            temp = month3 + "语音已超套；超出："+ str(over_gsm)+"分钟；产生额外语音费"+str(over_gsm_fee)+"元；"
            if tem not in first_chi and "近三个月" not in first_chi:
                first_chi.append(tem)
            if "y" not in flag:
                flag["y"] = "语音"
            scene_chi.append(temp)
        elif label_dict["three_last_month_gprs_over"] >= 1:
            over_gprs = self.tcw_gprs3
            over_gsm = self.tcw_gsm3
            over_gprs_fee = self.tcw_gprs_fee3
            over_gsm_fee = self.tcw_gsm_fee3
            tem = month3
            temp = month3 + "流量已超套；超出："+ str(over_gprs)+"G；产生额外流量费"+str(over_gprs_fee)+"元；"
            if tem not in first_chi and "近三个月" not in first_chi:
                first_chi.append(tem)
            if "L" not in flag:
                flag["L"] = "流量"
            scene_chi.append(temp)

        if len(first_chi) >= 2 :

            first_chi_str = ",".join(first_chi[:-1])+"和"+first_chi[-1]
        elif len(first_chi) != 0:
            first_chi_str = first_chi[-1]
        if label_dict["cur_month_gsm_will_over"] >= 1 and label_dict["cur_month_gprs_will_over"] >= 1:
            over_gprs = self.gprs_residua
            over_gsm = self.gsm_residua

            tem = "本月语音流量均预将超套;"
            temp = "本月语音流量均预将超套；仅剩余" + str(over_gprs)+ "G流量，剩余" + str(over_gsm) + "分钟语音;"
            #first_chi.append(tem)
            first_chi_str =tem+"以及"+first_chi_str
            scene_chi.append(temp)
        elif label_dict["cur_month_gsm_will_over"] >= 1 :
            over_gprs = self.gprs_residua
            over_gsm = self.gsm_residua

            tem = "本月语音预将超套;"
            temp = "本月语音预将超套；仅剩余" + str(over_gprs)+ "G流量，剩余" + str(over_gsm)+"分钟语音;"
            #first_chi.append(tem)
            first_chi_str =tem+"以及"+first_chi_str
            scene_chi.append(temp)
        elif label_dict["cur_month_gprs_will_over"] >= 1:
            over_gprs = self.gprs_residua
            over_gsm = self.gsm_residua
            tem = "本月流量预将超套;"
            temp = "本月流量预将超套；仅剩余" + str(over_gprs)+ "G流量，剩余" + str(over_gsm)+"分钟语音;"
            #first_chi.append(tem)
            first_chi_str =tem+first_chi_str
            scene_chi.append(temp)
        temp = self.activates(label_dict)
        first_chi.append(temp)
        scene_chi.append(temp)
        if len(flag) == 2:   
            first_chi_str = "您好，关注到您" +first_chi_str + "语音流量均有超套情况，" +  temp
        elif len(flag) == 1 :
            for k in flag:
                first_chi_str = "您好，关注到您" +first_chi_str + flag[k] + "有超套情况，" +  temp
        else:
            first_chi_str = "您好，关注到您" + first_chi_str + temp[1:]
        scene_chi_str = "您好，关注到您近几个月的套餐使用," + "".join(scene_chi)
        return first_chi_str,scene_chi_str
    #标签匹配，输出命中的标签及切入语，场景话术；
    def label_match(self) :
        label_dict = self.pre_label()

        for key in self.label_list:
            if self.label_list[key] >= 1 and label_dict[key] >= 1:
                label_dict[key] = 1
            else:
                label_dict[key] = 0
        flag = 0
        for key in label_dict:
            if label_dict[key] == 1:
                flag = 1
                break
        if flag == 0:
            first_chi_str = "您好，关注到您近几个月的套餐使用情况，"
            scene_chi_str = "您好，关注到您近几个月的套餐使用情况，"
            return label_dict,first_chi_str,scene_chi_str
        first_chi_str,scene_chi_str = self.product_chinese(label_dict)

        return label_dict,first_chi_str,scene_chi_str
    def out_content(self):
        label_dict,first_chi_str,scene_chi_str = self.label_match()
        key_v = {'cur_month_gprs_over': "当月流量已超套", 'over_activity': "活动有到期", 'two_last_month_gprs_over': "账务月减一流量超套", 
    'next_day_num': "次日有办理的活动将到期", 'pre_day_num': "上月有办理的活动已到期", 'last_month_gsm_over': "账务月语音超套", 'last_month_gprs_over': "账务月流量超套", 
    'three_month_gsm_over': "近三个月语音均超套", 'three_month_gprs_over': "近三个月流量均超套", 'current_month_num': "本月有办理的活动已到期", 'cur_month_gsm_over': "当月语音已超套", 
    'next_month_num': "次月有办理的活动将到期", 'cur_month_gsm_will_over': "当月语音预将超套", 'cur_month_gprs_will_over': "当月流量预将超套", 
    'two_last_month_gsm_over': "账务月减一语音超套",'three_last_month_gprs_over': "账务月减二流量超套", 'three_last_month_gsm_over': "账务月减二语音超套"}
        res = []
        #print(label_dict)
        for k in label_dict:
            
            if label_dict[k] == 1:
                #print(k,label_dict[k])
                res.append(str(key_v[k]))
        res_dic = {}
        #print(str(res).decode('string_escape'))
        #print(len(res),len(label_dict))
        res_dic["user_label"] = res
        res_dic["label_rate"] = float(len(res)/float(len(label_dict)))
        res_dic["first_script"] = first_chi_str
        res_dic["market_script"] = first_chi_str + self.productDesc
        return res_dic

if __name__=='__main__':

    #data = { 
     #   "is_gprs_over" : 1,"is_gsm_over" : 1,"gprs_all" : 20,"gsm_all" : 80,
    #    "gprs_residua" : 0,"gsm_residua" : 0,"avg_day_gprs" : 0.11,"avg_day_gsm" : 5,
    #    "suf_current_day_count" : 10,"avg_three_month_gsm_count" : 3, "avg_three_month_gprs_count" : 3,
    #    "avg_three_month_gprs_fee": 10,"avg_three_month_gsm_fee":5,"tcw_gprs_fee1" :3,
    #    "tcw_gsm_fee1" : 4,"gprs_all1" : 30,"gprs_main_all1" : 20,"tcw_gprs1" : 10000/1024,
    #    "tcw_gprs_fee3":0,
    #    "tcw_gsm_fee3":0,
    #    "tcw_gprs3":0,
    #    "tcw_gsm3":0,
    #    "gsm_zj1" : 100,"tcw_gsm1": 30,"gsm_main_all1" : 70,"tcw_gprs_fee2" : 20,"tcw_gsm_fee2" : 15,
    #    "tcw_gprs2" : 20,"tcw_gsm2" : 20,"is_over_activity" : 1, "pre_current_day_count" : 1,"current_month_count" : 1,
    #    "next_day_count" :1,"next_month_count" : 1,"now_prod_value" :128,"now_prod_gprs" : 70,"now_prod_gsm" : 80,
    #    "gprs_all" : 80,"gsm_all" :90,"avg_three_month_gsm" : 10,"avg_three_month_gprs":10,
    #    "productDesc":"现为你推荐一款更优惠的套餐，我为你介绍一下，套餐原价69元/月，折扣后仅需59元，包含20G流量，400分钟通话"
    #}
    data = { 
        "is_gprs_over" : 0,"is_gsm_over" : 0,"gprs_all" : 0,"gsm_all" : 0,
        "gprs_residua" : 0,"gsm_residua" : 0,"avg_day_gprs" : 0,"avg_day_gsm" : 0,
        "suf_current_day_count" : 0,"avg_three_month_gsm_count" : 3, "avg_three_month_gprs_count" : 3,
        "avg_three_month_gprs_fee": 0,"avg_three_month_gsm_fee":0,"tcw_gprs_fee1" :0,
        "tcw_gsm_fee1" : 0,"gprs_all1" : 0,"gprs_main_all1" : 0,"tcw_gprs1" : 0,
        "tcw_gprs_fee3":0,
        "tcw_gsm_fee3":0,
        "tcw_gprs3":0,
        "tcw_gsm3":0,
        "gsm_zj1" : 0,"tcw_gsm1": 0,"gsm_main_all1" : 0,"tcw_gprs_fee2" : 0,"tcw_gsm_fee2" : 0,
        "tcw_gprs2" : 0,"tcw_gsm2" : 0,"is_over_activity" : 0, "pre_current_day_count" : 0,"current_month_count" : 0,
        "next_day_count" :0,"next_month_count" : 0,"now_prod_value" :0,"now_prod_gprs" : 0,"now_prod_gsm" : 0,
        "gprs_all" : 0,"gsm_all" :0,"avg_three_month_gsm" : 0,"avg_three_month_gprs":0,
        "productDesc":"现为你推荐一款更优惠的套餐，我为你介绍一下，套餐原价69元/月，折扣后仅需59元，包含20G流量，400分钟通话"
    }
    print("start")
    p = process(data)
    values= p.out_content()
    print("匹配的用户标签：")
    print(str(values).decode('string_escape'))
    #print(str(res).decode('string_escape'))
    #print(key_v)
    #print(label_dict)
    #label_dict,first_chi_str,scene_chi_str = p.label_match()
    '''print("生成的切入语：")
    print(first_chi_str)
    print("生成的场景话术(产品信息暂缺)：")
    print(first_chi_str+"现为你推荐一款更优惠的套餐，我为你介绍一下，套餐原价69元/月，折扣后仅需59元，包含20G流量，400分钟通话，同时送40G流量，200分钟通话")'''


        
 

        
     
        














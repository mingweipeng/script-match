import os
import datetime
def load_conf_labels():
    label_id={}
    label_name={}
    label_dic={}
    with open("label.conf","r") as fr:
        for line in fr:
            values=line.strip().split("_")
            if len(values) !=3 or str(values[2])!= "1":
                continue
            #print(values)
            #all_labels.append({"labelName":values[0],"labelId":values[1]})
            label_id[values[0]]=values[1]
    #print(len(label_id))
    with open("label_english_name.conf","r") as fr:
        for line in fr:
            values=line.strip().split("@")
            #print(values)
            if len(values) !=2:
                #print(values)
                continue
            #print(values)
            #all_labels.append({"labelName":values[0],"labelId":values[1]})
            label_name[values[0]]=values[1]
    #print(len(label_name))
    for key in label_id:
        if key in label_name:
            #print(label_name[key])
            label_dic[label_name[key]] = 0
        #else:
            #print(key)
    #print(len(label_dic))
    return label_dic

class predict():
    def __init__(self,data,maxvalue):
        #活动减免额度
        self.now_reduce_price = data['now_reduce_price']
        #是否流量超套餐
        self.is_gprs_over = data["is_gprs_over"]
        #是否语音超套餐
        self.is_gsm_over = data["is_gsm_over"]
        #当月总使用流量
        self.gprs_all = data["gprs_all"]
        #当月总使用语音
        self.gsm_all = data["gsm_all"]
        #当月剩余流量
        self.gprs_residua = data["gprs_residua"]
        #当月剩余语音
        self.gsm_residua = data["gsm_residua"]
        #当月日均使用流量
        self.avg_day_gprs = data["avg_day_gprs"]
        #当月日均使用语音
        self.avg_day_gsm = data["avg_day_gsm"]
        #当前距月末几天
        self.suf_cur_day_count = data["suf_current_day_count"]
        #近三个月语音超套次数
        self.avg_three_month_gsm_count = data["avg_three_month_gsm_count"]
        #近三个月流量超套次数
        self.avg_three_month_gprs_count = data["avg_three_month_gprs_count"]
        #近三个月平均套外流量费
        self.avg_three_month_gprs_fee = data["avg_three_month_gprs_fee"]
        #近三个月平均套外语音费
        self.avg_three_month_gsm_fee = data["avg_three_month_gsm_fee"]
        #近三个月平均套外语音使用量
        self.avg_three_month_gsm = data["avg_three_month_gsm"]
        #近三个月平均套外流量使用量
        self.avg_three_month_gprs = data["avg_three_month_gprs"]
        #账务月套外上网费
        self.tcw_gprs_fee1 = data["tcw_gprs_fee1"]
        #账务月套外语音费
        self.tcw_gsm_fee1 = data["tcw_gsm_fee1"]
        #账外月套内总使用流量
        self.gprs_main_all1 = data["gprs_main_all1"]
        self.gprs_main_all2 = data["gprs_main_all2"]
        self.gprs_main_all3 = data["gprs_main_all3"]
        #账务月套外上网使用量
        self.tcw_gprs1 = data["tcw_gprs1"]
        self.gsm_main_all1 = data['gsm_main_all1']
        self.gsm_main_all2 = data['gsm_main_all2']
        self.gsm_main_all3 = data['gsm_main_all3']
        #账务月总使用语音量
        self.gsm_zj1 = data["gsm_zj1"]
        self.gsm_zj2 = data["gsm_zj2"]
        self.gsm_zj3 = data["gsm_zj3"]
        #账务月套外总使用语音量
        self.tcw_gsm1 = data["tcw_gsm1"]
        #账务月减一套外上网费
        self.tcw_gprs_fee2 = data["tcw_gprs_fee2"]
        #账务月减一套外语音费
        self.tcw_gsm_fee2 = data["tcw_gsm_fee2"]
        #账务月减一套外上网流量
        self.tcw_gprs2 = data["tcw_gprs2"]
        #账务月减一套外语音使用量
        self.tcw_gsm2 = data["tcw_gsm2"]
        #账务月减二套外上网费
        self.tcw_gprs_fee3 = data["tcw_gprs_fee3"]
        #账务月减二套外语音费
        self.tcw_gsm_fee3 = data["tcw_gsm_fee3"]
        #账务月减二套外上网流量
        self.tcw_gprs3 = data["tcw_gprs3"]
        #账务月减二套外语音使用量
        self.tcw_gsm3 = data["tcw_gsm3"]
        #是否有活动到期
        self.over_activity = data["is_over_activity"]
        #上个月失效活动个数
        self.pre_day_count = data["pre_day_num"]
        #本月失效活动个数
        self.current_month_count = data["current_month_num"]
        #次日失效活动个数
        self.next_day_count = data["next_day_num"]
        #次月失效活动个数
        self.next_month_count = data["next_month_num"]
        #主套餐资费
        self.now_prod_value = data["now_prod_value"]
        #主套餐包含流量
        self.now_prod_gprs = data["now_prod_gprs"]
        #主套餐包含语音
        self.now_prod_gsm = data["now_prod_gsm"]
        #当月总使用流量
        self.gprs_all = data["gprs_all"]
        self.gprs_all1 = data["gprs_all1"]
        self.gprs_all2 = data["gprs_all2"]
        self.gprs_all3 = data["gprs_all3"]
        self.bill_fee1 =data["bill_fee1"]
        self.bill_fee2 =data["bill_fee2"]
        self.bill_fee3 =data["bill_fee3"]
        #当月总使用语音
        self.gsm_all = data["gsm_all"]
        #self.data_content = data["data_content"]
        self.productDesc = data["productDesc"]
        #坐席勾选的场景标签
        self.label_list = {"cur_month_gsm_over":1,"cur_month_gprs_over":1,"two_last_month_gsm_over":1,"two_last_month_gprs_over":1,"cur_month_gsm_will_over":1,"cur_month_gprs_will_over":1,"three_month_gsm_over":1,"three_month_gprs_over":1,"last_month_gsm_over":1,"last_month_gprs_over":1,"over_activity":1,"pre_day_num":1,"current_month_num":1,"next_day_num":1,"next_month_num":1}
    #当月语音是否超套
        self.label_name_value =maxvalue
    
    def is_high_user(self,set_now_prod_value=12800):
        if self.now_prod_value > set_now_prod_value and self.bill_fee1 >= self.now_prod_value and (self.gprs_all1 > self.now_prod_gprs or self.gsm_zj1 >self.now_prod_gsm):
            return True
        return False
    def is_low_gsm_gprs(self):
        if self.now_prod_gprs < 3000 and self.now_prod_gsm < 40:
            if (self.now_prod_gprs < self.gprs_main_all1 or self.now_prod_gsm <self.gsm_main_all1) and (self.gsm_residua < 0.5*self.now_prod_gsm or self.gsm_residua < 0.5*self.now_prod_gsm):
                return True
            if (self.now_prod_gprs - self.gprs_main_all1 < 0.1*self.now_prod_gprs or self.now_prod_gsm -self.gsm_main_all1 < 0.1*self.now_prod_gsm) and (self.gsm_residua < 0.5*self.now_prod_gsm or self.gsm_residua < 0.5*self.now_prod_gsm):
                return True
        return False
    def is_gprs_used_growth(self):
        if self.gprs_all1 > self.gprs_all2 and self.gprs_all2 > self.gprs_all3 and self.gprs_residua < 0.5*self.now_prod_gprs:
            return True
        return False
    def is_gsm_used_growth(self):
        #if self.now_prod_gsm-self.gsm_main_all1 > self.gsm_main_all1 -self.gsm_main_all2:
        if self.gsm_zj1 > self.gsm_zj2 and self.gsm_zj2 > self.gsm_zj3 and self.gsm_residua < 0.5*self.now_prod_gsm:
            return True
        return False
    def is_high_gprs_low_gsm(self):
        #if (self.now_prod_gprs - self.gprs_main_all1)/self.now_prod_gprs > 0.4 and self.now_prod_gprs >15000:
            #return True
        if self.now_prod_gsm - self.gsm_main_all1 <= 0.2*self.now_prod_gsm and self.now_prod_gsm - self.gsm_main_all2 <= 0.2*self.now_prod_gsm and self.now_prod_gsm - self.gsm_main_all3 <= 0.2*self.now_prod_gsm:
            if self.gsm_residua < 0.5*self.now_prod_gsm:
                return True
        return False
    def is_high_gsm_low_gprs(self):
        #if (self.now_prod_gsm - self.gsm_main_all1)/self.now_prod_gsm > 0.4 and self.now_prod_gsm >20:
            #return True
        if self.now_prod_gprs - self.gprs_main_all1 <= 0.2*self.now_prod_gprs and self.now_prod_gprs - self.gsm_main_all2 <= 0.2*self.now_prod_gprs and self.now_prod_gprs - self.gsm_main_all3 <= 0.2*self.now_prod_gprs:
            if self.gprs_residua < 0.5*self.now_prod_gprs:
                return True
        return False
    def is_reduce_user(self):
        if self.now_prod_value> 1900 and self.now_reduce_price/self.now_prod_value >0.10 and (self.gprs_residua < 0.5*self.now_prod_gprs or self.gsm_residua < 0.5*self.now_prod_gsm):
            return True
        return False
    #账务月流量使用大于主套餐包含流量
    def is_gprs_over_now_prod(self):
        if self.gprs_all1 > self.now_prod_gprs and self.tcw_gprs_fee1 <= 0 and self.gprs_residua < 0.5*self.now_prod_gprs:
            return True
        return False
    #账务月语音使用大于主套餐包含语音
    def is_gsm_over_now_prod(self):
        if self.gsm_zj1 > self.now_prod_gsm and self.tcw_gsm_fee1 <= 0 and self.gsm_residua < 0.5*self.now_prod_gsm:
            return True
        return False
    def is_cur_month_gprs_over(self,gprs_over_value =1):
        #print(gprs_over_value)
        if self.is_gprs_over >= 1 and gprs_over_value == 1:
            return True
        elif self.gprs_all - self.now_prod_gprs >= gprs_over_value and gprs_over_value !=1:
            return True
        return False
    #当月流量是否超套
    def is_cur_month_gsm_over(self,gsm_over =1):
        if self.is_gsm_over >=1 and gsm_over==1:
            return True
        elif self.gsm_all- self.now_prod_gsm>= gsm_over and gsm_over !=1:
            return True
        return False
    #账务月流量超套
    def is_last_month_gprs_over(self,tcw_gprs1_value=0):
        if  self.tcw_gprs1 > tcw_gprs1_value:
            return True
        if self.tcw_gprs_fee1 > 0 and tcw_gprs1_value==0:
            return True
        return False
    #账务月语音超套
    def is_last_month_gsm_over(self,tcw_gsm1_value =0): 
        if  self.tcw_gsm1 > tcw_gsm1_value:
            return True
        if self.tcw_gsm_fee1 > 0 and tcw_gsm1_value==0:
            return True
        return False
    #近三个月流量均超套
    def is_three_month_gprs_over(self):
        if self.avg_three_month_gprs_count >= 3 :
            return True
        return False
    #近三个月语音均超套
    def is_three_month_gsm_over(self):
        if self.avg_three_month_gsm_count >= 3 :
            return True
        return False
    #当月流量预将超套
    def is_cur_month_gprs_will_over(self):
        #print("当月流量预将超套")
        if self.now_prod_gprs <= 0 or self.suf_cur_day_count <= 0:
            return False
        if self.gprs_residua/self.now_prod_gprs < 0.3 and self.gprs_residua / self.suf_cur_day_count < self.avg_day_gprs and self.gprs_residua / self.suf_cur_day_count < 0.01:
            return True
        return False
    #当月语音预将超套
    def is_cur_month_gsm_will_over(self):
        #print("当月语音预将超套")
        if self.now_prod_gsm <= 0 or self.suf_cur_day_count <= 0:
            return False
        if self.gsm_residua/self.now_prod_gsm < 0.15 and self.gsm_residua / self.suf_cur_day_count < self.avg_day_gsm and self.gsm_residua/self.suf_cur_day_count < 2 :
            return True
        return False
    def is_will_over_level(self):
        if self.suf_cur_day_count<=10:
            return 1
        elif self.suf_cur_day_count>=10 and self.suf_cur_day_count<=20:
            return 2
        else:
            return 3
    #上个账务月流量超套
    def is_two_last_month_gprs_over(self,tcw_gprs2_value =0 ):
        if self.tcw_gprs2 > tcw_gprs2_value:
            return True
        if self.tcw_gprs_fee2 > 0 and tcw_gprs2_value ==0:
            return True
        return False
    
    #上个账务月语音超套
    def is_two_last_month_gsm_over(self,tcw_gsm2_value=0): 
        if  self.tcw_gsm2 > tcw_gsm2_value:
            return True
        if self.tcw_gsm_fee2 > 0 and tcw_gsm2_value==0:
            return True
        return False
    #上上个帐务月流量超套
    def is_three_last_month_gprs_over(self,tcw_gprs3_value=0):
        if self.tcw_gprs3 > tcw_gprs3_value :
            return True
        if self.tcw_gprs_fee3 > 0 and tcw_gprs3_value==0:
            return True
        return False
    
    #上上个账务月语音超套
    def is_three_last_month_gsm_over(self,tcw_gsm3_value = 0):
        if  self.tcw_gsm3 > tcw_gsm3_value:
            return True
        if self.tcw_gsm_fee3 > 0 and tcw_gsm3_value == 0:
            return True
        return False
    #超套类level计算
    def is_over_fee_level(self,over_fee):
        if over_fee <= 1:
            return 1
        elif over_fee >= 1 and over_fee >= 1 and over_fee <= 5:
            return 2
        else:
            return 3
    
    #活动到期
    def is_over_activity(self):
        if self.pre_day_count > 0 or self.current_month_count > 0 or self.next_day_count > 0 or self.next_month_count > 0:
            return 1
        return 0
    #上月有办理的活动已到期
    def is_pre_day_num(self):
        if  self.pre_day_count > 0:
            #return self.pre_day_count
            return 1
        return 0
    #本月有办理的活动将到期
    def is_current_month_num(self):
        if  self.current_month_count > 0:
            #return self.current_month_count
            return 1
        return 0
    #次日有办理的活动将到期
    def is_next_day_num(self):
        if self.next_day_count>0:
            return 1
        return 0
    #次月有办理的活动将到期
    def is_next_month_num(self):
        if self.next_month_count>0:
            return 1
        return 0
  
    def predict_label(self):
        '''label_dict = {"cur_month_gsm_over":0,"cur_month_gprs_over":0,"two_last_month_gsm_over":0,
                      "two_last_month_gprs_over":0,"cur_month_gsm_will_over":0,"cur_month_gprs_will_over":0,
                    "three_month_gsm_over":0,"three_month_gprs_over":0,"last_month_gsm_over":0,
                    "last_month_gprs_over":0,"pre_day_num":0,"current_month_num":0,
                    "next_day_num":0,"next_month_num":0,
                    "three_last_month_gsm_over":0,
                    "three_last_month_gprs_over":0             
                    }'''
        label_dict = load_conf_labels()
        if "cur_month_gsm_over" in self.label_name_value and self.is_cur_month_gsm_over(self.label_name_value["cur_month_gsm_over"]):
            level = self.is_will_over_level()
            label_dict["cur_month_gsm_over"] = level
        elif self.is_cur_month_gsm_over():
            level = self.is_will_over_level()
            label_dict["cur_month_gsm_over"] = level
        else:
            if self.is_cur_month_gsm_will_over():
                level = self.is_will_over_level()
                label_dict["cur_month_gsm_will_over"] = level
        #print(self.label_name_value)
        if "cur_month_gprs_over" in self.label_name_value and self.is_cur_month_gprs_over(self.label_name_value["cur_month_gprs_over"]):
            #print(self.label_name_value["cur_month_gprs_over"])
            #print("start11")
            level = self.is_will_over_level()
            label_dict["cur_month_gprs_over"] = level
        elif self.is_cur_month_gprs_over() and "cur_month_gprs_over" not in self.label_name_value:
            #print("start22")
            level = self.is_will_over_level()
            label_dict["cur_month_gprs_over"] = level
        else:
            if self.is_cur_month_gprs_will_over():
                level = self.is_will_over_level()
                label_dict["cur_month_gprs_will_over"] = level
        if self.is_three_month_gprs_over():
            level = self.is_over_fee_level(self.tcw_gprs_fee1+self.tcw_gprs_fee2+self.tcw_gprs_fee3)
            label_dict["three_month_gprs_over"] = level
        else:
            if self.is_last_month_gprs_over():
                level = self.is_over_fee_level(self.tcw_gprs_fee1)
                label_dict["last_month_gprs_over"] = level
            if self.is_two_last_month_gprs_over():
                level = self.is_over_fee_level(self.tcw_gprs_fee2)
                label_dict["two_last_month_gprs_over"] = level
            if self.is_three_last_month_gprs_over():
                level = self.is_over_fee_level(self.tcw_gprs_fee3)
                label_dict["three_last_month_gprs_over"] = level

        if self.is_three_month_gsm_over():
            level = self.is_over_fee_level(self.tcw_gsm_fee1+self.tcw_gsm_fee2+self.tcw_gsm_fee3)
            label_dict["three_month_gsm_over"] = level        
        else:
            if self.is_last_month_gsm_over():
                level = self.is_over_fee_level(self.tcw_gsm_fee1)
                label_dict["last_month_gsm_over"] = level
            if self.is_two_last_month_gsm_over():
                level = self.is_over_fee_level(self.tcw_gsm_fee2)
                label_dict["two_last_month_gsm_over"] = level
            if self.is_three_last_month_gsm_over():
                level = self.is_over_fee_level(self.tcw_gsm_fee3)
                label_dict["three_last_month_gsm_over"] = level
        #label_dict["over_activity"] = self.is_over_activity()
        label_dict["over_activity"] = 0
        label_dict["pre_day_num"] = self.is_pre_day_num()
        label_dict["current_month_num"] = self.is_current_month_num()
        label_dict["next_day_num"] = self.is_next_day_num()
        label_dict["next_month_num"] = self.is_next_month_num()
        
        if "high_prod_user" in label_dict and self.is_high_user():
            label_dict["high_prod_user"] = 1
        if "low_gsm_gprs" in label_dict and self.is_low_gsm_gprs():
            label_dict["low_gsm_gprs"] = 1
        if "gprs_used_growth" in label_dict and self.is_gprs_used_growth():
            label_dict["gprs_used_growth"] = 1
        if "gsm_used_growth" in label_dict and self.is_gsm_used_growth():
            label_dict["gsm_used_growth"] = 1
        if "high_gprs_low_gsm" in label_dict and self.is_high_gprs_low_gsm():
            label_dict["high_gprs_low_gsm"] = 1
        if "high_gsm_low_gprs" in label_dict and self.is_high_gsm_low_gprs():
            label_dict["high_gsm_low_gprs"] = 1
        if "reduce_user" in label_dict and self.is_reduce_user():
            label_dict["reduce_user"] = 1
        if "gprs_over_now_prod" in label_dict and self.is_gprs_over_now_prod():
            label_dict["gprs_over_now_prod"] = 1
        if "gsm_over_now_prod" in label_dict and self.is_gsm_over_now_prod():
            label_dict["gsm_over_now_prod"] = 1
        #print(label_dict)
        #print("ending predict")
        return label_dict
def merge_two_str(str1,str2):
    #print(str1,str2)
    mid_str=""
    if len(str1)!=len(str2):
        mid_str=","
    if str1[0]!=str2[0]:
        return str1+","+str2
    index=[]
    flag=True
    #print(len(str1))
    for i in range(len(str1)):
        #print(i)
        if flag==True:
            if str1[i]==str2[i]:            
                continue
            else:
                index.append(i)
                #print(index)
                flag=False
        else:
            #print(str1[i],str2[i])
            if str1[i]!=str2[i]:
                #print(str1[i],str2[i])
                continue
            else:
                index.append(i)
                flag=True
    start=0
    index.append(len(str1))
    merge_strs=""
    #print(index)
    for j,end in enumerate(index):
        #print(j%2)
        if j%2==0:
            merge_strs+=str1[start:end]
            start=end
        else:
            merge_strs+=str1[start:end]+mid_str+str2[start:end]
            start=end
    if start<len(str2):
        merge_strs+=str2[start:]
    #print(merge_strs)
    return merge_strs
        
def merge_str():
    contents = ["上月参与了主套餐优惠减免活动"]
    #"近几个月的语音使用连续增长","近几个月的流量使用连续增长","上月套餐使用流量剩余量偏多而语音偏少",
    #"上月套餐使用语音剩余量偏多而流量偏少","上月流量使用超出主套餐包含的流量","上月语音使用超出主套餐包含的语音"]
    contents1=[]
    flag =[]
    for i in range(len(contents)):
        if i in flag:
            continue
        first_str=contents[i]
        second_str=""
        for j in range(i+1,len(contents)):
            if len(contents[j])!=len(contents[i]) or contents[j][0]!=contents[i][0]:
                continue
            second_str=contents[j]
            #print(first_str,second_str)
            product_str=merge_two_str(first_str,second_str)
            contents1.append(product_str)
            flag.append(j)
            break
        if second_str=="":
            contents1.append(first_str)
    print(contents1)
    #二次融合
    contents2=[]
    flag =[]
    for i in range(len(contents1)):
        if i in flag:
            continue
        first_str=contents1[i]
        product_str=first_str
        second_str=""
        for j in range(i+1,len(contents1)):
            if  contents1[j][0]!=contents1[i][0]:
                continue
            second_str=contents1[j]
            if len(first_str)<=len(second_str):
                product_str=merge_two_str(first_str,second_str)
            else:
                product_str=merge_two_str(second_str,first_str)
            #contents2.append(product_str)
            flag.append(j)
            
        contents2.append(product_str)
    final_str=",".join(contents2)
    print(final_str)


if __name__=='__main__':
    print("check predict.....")
    #all_labels = load_conf_labels()
    #str1="上月参与了主套餐优惠减免活动"
    #str2="套餐使用语音剩余量偏多而流量偏少"
    #str2="上月参与了主套餐优惠减免活动"
    #merge_two_str(str1,str2)
    merge_str()
    #print(all_labels)

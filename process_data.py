import os
from match_and_merge import match_merge
from match_and_merge import load_conf_labels

class process_data():
    def __init__(self,data_content):
        self.data =data_content['data']
        self.label_list =data_content['label_list']
        self.top_count =data_content['top_count']
        self.maxvalue =data_content['maxvalue']
    def out_content(self):
        b = match_merge(self.data,self.label_list,self.top_count,self.maxvalue)
        p=b.out_content()
        return p
    
class get_scene_labels():
    def __init__(self,data_flag):
        self.get_all_labels = data_flag["get_all_labels"]
    def get_labels(self):
        out_dict = {}
        if self.get_all_labels !=1:
            return out_dict
        all_label = load_conf_labels()
        out_dict["all_labels"] = all_label
        #print(out_dict)
        return out_dict
class process():
    def __init__(self,data):
        self.data = data
    def deal_with(self):
        if "get_all_labels" in self.data:
            b=get_scene_labels(self.data)
            p=b.get_labels()
            return p
        top_count = self.data['top_count']
        if top_count == "":
            top_count = 5
        label_level = self.data["label_level"]
        maxvalue = {}
        data_content = self.data
        b = match_merge(data_content,label_level,top_count,maxvalue)
        p=b.out_content()
        return p
    def out_content(self):
        content = self.deal_with()
        return content
if __name__=='__main__':
    data = { 
        "is_gprs_over" : 0,"is_gsm_over" : 0,"gprs_all" : 0,"gsm_all" : 0,
        "gprs_residua" : 10,"gsm_residua" : 10,"avg_day_gprs" : 20,"avg_day_gsm" : 20,
        "suf_current_day_count" : 20,"avg_three_month_gsm_count" : 0, "avg_three_month_gprs_count" : 0,
        "avg_three_month_gprs_fee": 0,"avg_three_month_gsm_fee":0,"tcw_gprs_fee1" :0,
        "tcw_gsm_fee1" : 0,"gprs_all1" : 0,"gprs_main_all1" : 0,"tcw_gprs1" : 0,
        "tcw_gprs_fee3":0,
        "tcw_gsm_fee3":10,
        "tcw_gprs3":0,
        "tcw_gsm3":10,
        "gsm_zj1" : 0,"tcw_gsm1": 0,"gsm_main_all1" : 0,"tcw_gprs_fee2" : 0,"tcw_gsm_fee2" : 0,
        "tcw_gprs2" : 0,"tcw_gsm2" : 0,"is_over_activity" : 0, "pre_day_num" : 0,"current_month_num" : 0,
        "next_day_num" :0,"next_month_num" : 0,"now_prod_value" :0,"now_prod_gprs" : 100,"now_prod_gsm" : 100,
        "gprs_all" : 200,"gsm_all" :100,"avg_three_month_gsm" : 0,"avg_three_month_gprs":0,
        "productDesc":"现为你推荐一款更优惠的套餐，我为你介绍一下，套餐原价69元/月，折扣后仅需59元，包含20G流量，400分钟通话"
    }
    data1 = { 
        "is_gprs_over" : 1,"is_gsm_over" : 0,"gprs_all" : 0,"gsm_all" : 0,"now_reduce_price":0,
        "gprs_residua" : 10,"gsm_residua" : 10,"avg_day_gprs" : 20,"avg_day_gsm" : 20,
        "suf_current_day_count" : 20,"avg_three_month_gsm_count" : 2, "avg_three_month_gprs_count" : 3,
        "avg_three_month_gprs_fee": 0,"avg_three_month_gsm_fee":0,"tcw_gprs_fee1" :0,
        "tcw_gsm_fee1" : 10,"gprs_all1" : 0,"gprs_all2" : 0,"gprs_all3" : 0,"gprs_main_all1" : 0,"gprs_main_all2" : 0,"gprs_main_all3" : 0,"tcw_gprs1" : 0,
        "tcw_gprs_fee3":0,
        "tcw_gsm_fee3":10,
        "tcw_gprs3":0,
        "tcw_gsm3":0,
        "gsm_zj1" : 0,"tcw_gsm1": 0,"gsm_main_all1" : 0,"gsm_main_all2" : 0,"gsm_main_all3" : 0,"tcw_gprs_fee2" : 0,"tcw_gsm_fee2" : 0,
        "tcw_gprs2" : 0,"tcw_gsm2" : 0,"is_over_activity" : 0, "pre_day_num" : 3,"current_month_num" : 3,
        "next_day_num" :3,"next_month_num" : 1,"now_prod_value" :19200,"now_prod_gprs" : 100,"now_prod_gsm" : 100,
        "gprs_all" : 100,"gsm_all" :100,"avg_three_month_gsm" : 0,"avg_three_month_gprs":0,
        "productDesc":"现为你推荐一款更优惠的套餐，我为你介绍一下，套餐原价69元/月，折扣后仅需59元，包含20G流量，400分钟通话"
    }
    label_list =  {"cur_month_gsm_over":3,"cur_month_gprs_over":1,"two_last_month_gsm_over":1,"two_last_month_gprs_over":1,
    "cur_month_gsm_will_over":1,"cur_month_gprs_will_over":1,"three_month_gsm_over":1,"three_month_gprs_over":1,
    "last_month_gsm_over":1,"last_month_gprs_over":1,"over_activity":1,"pre_day_num":1,"current_month_num":1,
    "next_day_num":1,"next_month_num":3}
    label_list1 = {"账务月减一流量超套":2,"账务月语音超套":3,"账务月流量超套":2,"近三个月语音均超套":1,"近三个月流量均超套":1,
    "当月语音已超套":1,"当月语音预将超套":1,"当月流量预将超套":1,"账务月减一语音超套":3,"账务月减二流量超套":2,"账务月减二语音超套":3,"当月流量已超套":1}
    top_count = 3
    label_list2 = {}
    maxvalue = {"cur_month_gprs_over":500}
    #b = match_merge(data1,label_list,top_count,maxvalue)
    data1['top_count'] = top_count
    data1['label_level'] = label_list2
    da = {"get_all_labels":1}
    data2 ={
        "is_gprs_over" : 0,"is_gsm_over" : 1,"gprs_all" : 0,"gsm_all" : 0,"now_reduce_price":0,
        "gprs_residua" : 10,"gsm_residua" : 10,"avg_day_gprs" : 20,"avg_day_gsm" : 20,
        "suf_current_day_count" : 20,"avg_three_month_gsm_count" : 2, "avg_three_month_gprs_count" : 3,
        "avg_three_month_gprs_fee": 0,"avg_three_month_gsm_fee":0,"tcw_gprs_fee1" :0,
        "tcw_gsm_fee1" : 10,"gprs_all1" : 0,"gprs_all2" : 0,"gprs_all3" : 0,"gprs_main_all1" : 0,"gprs_main_all2" : 0,"gprs_main_all3" : 0,"tcw_gprs1" : 0,
        "tcw_gprs_fee3":0,
        "tcw_gsm_fee3":10,
        "tcw_gprs3":0,
        "tcw_gsm3":0,
        "gsm_zj1" : 0,"tcw_gsm1": 0,"gsm_main_all1" : 0,"gsm_main_all2" : 0,"gsm_main_all3" : 0,"tcw_gprs_fee2" : 0,"tcw_gsm_fee2" : 0,
        "tcw_gprs2" : 0,"tcw_gsm2" : 0,"is_over_activity" : 0, "pre_day_num" : 1,"current_month_num" : 1,
        "next_day_num" :1,"next_month_num" : 1,"now_prod_value" :19200,"now_prod_gprs" : 100,"now_prod_gsm" : 100,
        "gprs_all" : 100,"gsm_all" :100,"avg_three_month_gsm" : 0,"avg_three_month_gprs":0,
        "productDesc":"现为你推荐一款更优惠的套餐，我为你介绍一下，套餐原价69元/月，折扣后仅需59元，包含20G流量，400分钟通话",
        "top_count":"4",
        "label_level":{"账务月减一流量超套":2,"账务月语音超套":3,"账务月流量超套":2,"近三个月语音均超套":1,"近三个月流量均超套":1,
    "当月语音已超套":3,"当月语音预将超套":1,"当月流量预将超套":1,"账务月减一语音超套":3,"账务月减二流量超套":2,"账务月减二语音超套":3,"当月流量已超套":3}
    }



    b=process(data2)
    p=b.out_content()
    print(p)
    '''da = {"get_all_labels":1}
    b=get_scene_labels(da)
    p=b.get_labels()
    print(p)'''
    print("start")
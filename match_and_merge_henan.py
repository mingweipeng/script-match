from predict_label import predict
class bcolors:
    HEADER = '\033[95m' #紫色
    OKBLUE = '\033[94m' #蓝色
    OKGREEN = '\033[92m' #翠绿
    WARNING = '\033[93m' #浅黄色
    FAIL = '\033[91m' #红色
    ENDC = '\033[0m' #默认值
    BOLD = '\033[1m' #加粗
    UNDERLINE = '\033[4m' #下斜线
def load_conf_labels():
    all_labels = []
    with open("label.conf","r") as fr:
        for line in fr:
            values=line.strip().split("_")
            #print(values)
            if len(values) !=3 or str(values[2])!= "1":
                #print(values)
                continue
            #print(values)
            all_labels.append({"labelName":values[0],"labelId":values[1]})
    return all_labels
def get_conf_labels_id():
    label_id={}
    label_name={}
    label_chi_eng={}
    with open("label.conf","r") as fr:
        for line in fr:
            values=line.strip().split("_")
            if len(values) !=3 or str(values[2])!= "1":
                continue
            label_id[values[0]]=values[1]
    with open("label_english_name.conf","r") as fr:
        for line in fr:
            values=line.strip().split("@")
            #print(values)
            if len(values) !=2:
                #print(values)
                continue
            #print(values)
            #all_labels.append({"labelName":values[0],"labelId":values[1]})
            label_name[values[1]]=values[0]
            label_chi_eng[values[0]] = values[1]
    return label_id,label_name,label_chi_eng

class match_merge():
    def __init__(self,data,label_lists,top_count,maxvalue):
        self.data = data
        self.predict = predict(data,maxvalue)
        self.key_id,self.key_v,self.label_chi_eng = get_conf_labels_id()
        self.label_list = {}
        for key in label_lists:
            if key in self.label_chi_eng:
                self.label_list[self.label_chi_eng[key]] = label_lists[key]
        self.label_level = {}
        self.maxvalue = maxvalue
        self.top_count = top_count
        self.productDesc = data["productDesc"]
    def match_label(self):
        label_dict = self.predict.predict_label()
        cross_label_dict = {}
        #print(self.label_list)
        #print(label_dict)
        if len(self.label_list) == 0:
            return label_dict
        for key in self.label_list:
            if self.label_list[key] >= 1 and label_dict[key] >= 1:
                cross_label_dict[key] = self.label_list[key]
            else:
                cross_label_dict[key] = 0
        for key in label_dict:
            if key not in cross_label_dict:
                cross_label_dict[key] = 0
        
        return cross_label_dict
    def merge_first_chinese(self,label_dict):
        first_chi = []
        first_script = []
        if label_dict["cur_month_gsm_over"] >= 1 and label_dict["cur_month_gprs_over"] >= 1:
            level = label_dict["cur_month_gsm_over"] + label_dict["cur_month_gprs_over"]
            first_chi.append({"time_pro":"当月","type":"语音流量","state":"均超套","level":level})
        elif label_dict["cur_month_gsm_over"] >= 1:
            level = label_dict["cur_month_gsm_over"]
            first_chi.append({"time_pro":"当月","type":"语音","state":"有超套","level":level})
        elif label_dict["cur_month_gprs_over"] >= 1:
            level = label_dict["cur_month_gprs_over"]
            first_chi.append({"time_pro":"当月","type":"流量","state":"有超套","level":level})
        
        if label_dict["three_month_gsm_over"] >= 1 and label_dict["three_month_gprs_over"] >= 1:
            level = label_dict["three_month_gsm_over"] + label_dict["three_month_gprs_over"] 
            first_chi.append({"time_pro":"近三个月","type":"语音流量","state":"均超套","level":level})
        elif label_dict["three_month_gprs_over"] >= 1:
            level = label_dict["three_month_gprs_over"] 
            first_chi.append({"time_pro":"近三个月","type":"流量","state":"均超套","level":level})
        elif label_dict["three_month_gsm_over"] >= 1:
            level = label_dict["three_month_gsm_over"] 
            first_chi.append({"time_pro":"近三个月","type":"语音","state":"均超套","level":level})
 
        if (label_dict["two_last_month_gsm_over"] >= 1 or label_dict["last_month_gsm_over"] >= 1 or label_dict["three_last_month_gsm_over"] >= 1) and (label_dict["two_last_month_gprs_over"] >= 1 or label_dict["last_month_gprs_over"] >= 1 or label_dict["three_last_month_gprs_over"] >= 1):
            level = label_dict["two_last_month_gsm_over"] + label_dict["last_month_gsm_over"] + label_dict["three_last_month_gsm_over"] + label_dict["two_last_month_gprs_over"] + label_dict["last_month_gprs_over"] + label_dict["three_last_month_gprs_over"]
            first_chi.append({"time_pro":"近三个月","type":"语音流量","state":"有超套","level":level})
        elif label_dict["two_last_month_gprs_over"] >= 1 or label_dict["last_month_gprs_over"] >= 1 or label_dict["three_last_month_gprs_over"] >= 1:
            level = label_dict["two_last_month_gprs_over"] + label_dict["last_month_gprs_over"] + label_dict["three_last_month_gprs_over"]
            first_chi.append({"time_pro":"近三个月","type":"流量","state":"有超套","level":level})
        elif label_dict["two_last_month_gsm_over"] >= 1 or label_dict["last_month_gsm_over"] >= 1 or label_dict["three_last_month_gsm_over"] >= 1:
            level = label_dict["two_last_month_gsm_over"] + label_dict["last_month_gsm_over"] + label_dict["three_last_month_gsm_over"]
            first_chi.append({"time_pro":"近三个月","type":"语音","state":"有超套","level":level}) 
        merge_first_chi = []
        merge_index = []
        last_index = 0
        for i in range(len(first_chi)):
            value = first_chi[i]
            if i in merge_index:
                continue
            flag = 0
            for j in range(i+1,len(first_chi)):
                if j in merge_index:
                    continue
                if value["time_pro"] != first_chi[j]["time_pro"] and value["type"] == first_chi[j]["type"]:
                    time_pro = value["time_pro"] +"及" + first_chi[j]["time_pro"]
                    types = value["type"]
                    state = ""
                    if value["state"] == "均超套" and first_chi[j]["state"] == "均超套":
                        state = "均超套"
                    elif (value["state"] == "均超套" and value["time_pro"] =="近三个月") or (first_chi[j]["state"] == "均超套" and first_chi[j]["time_pro"] =="近三个月"):
                        state = "均超套"
                    else:
                        state = "有超套"
                    level = value["level"] + first_chi[j]["level"] 
                    merge_first_chi.append({"time_pro":time_pro,"type":types,"state":state,"level":level})
                    flag = 1
                    merge_index.append(i)
                    merge_index.append(j)
                    break
                elif value["time_pro"] == first_chi[j]["time_pro"] and first_chi[j]["time_pro"] == "近三个月":
                    time_pro = value["time_pro"]
                    types = "语音流量"
                    state = "有超套"
                    level = value["level"] + first_chi[j]["level"] 
                    merge_first_chi.append({"time_pro":time_pro,"type":types,"state":state,"level":level})
                    flag = 1
                    merge_index.append(i)
                    merge_index.append(j)
                    break

            if i not in merge_index:
                merge_first_chi.append(value)
        if label_dict["cur_month_gsm_will_over"] >= 1 and label_dict["cur_month_gprs_will_over"] >= 1:
            level = label_dict["cur_month_gsm_will_over"] + label_dict["cur_month_gprs_will_over"]
            merge_first_chi.append({"time_pro":"当月","type":"语音流量","state":"均预超套","level":level})
        elif label_dict["cur_month_gsm_will_over"] >= 1:
            level = label_dict["cur_month_gsm_will_over"]
            merge_first_chi.append({"time_pro":"当月","type":"语音","state":"预超套","level":level})
        elif label_dict["cur_month_gprs_will_over"] >= 1:
            level = label_dict["cur_month_gprs_will_over"]
            merge_first_chi.append({"time_pro":"当月","type":"流量","state":"预超套","level":level})
        if  len(merge_first_chi) == 0:
            return first_script
        #for v in merge_first_chi:
            #vstr = v["time_pro"] + v["type"] + v["state"]
            #first_script.append(vstr)
        
        return merge_first_chi
    def activates(self,label_dict):
        
        first_str = "且办理的活动于"
        temp = ""
        if label_dict["pre_day_num"] >= 1:
            temp+="上月,"
        if label_dict["current_month_num"] >= 1:
            temp+="本月,"
        if temp != "":
            return first_str + temp[:-1] + "已到期，"
        if label_dict["next_day_num"]  >= 1:
            temp+="次日,"
        if label_dict["next_month_num"] >= 1:
            temp+="次月,"
        if temp != "":
            return first_str + temp[:-1] + "已到期，"

        return temp
    def merge_str(self,first_script):
        #first_str = []
        first_str = {}
        for v in first_script:
            vstr =  "<span style= 'color:red'>" + v["time_pro"] + v["type"] + v["state"] + "</span>"
            #print(vstr)
            #first_str.append(vstr)  
            first_str[vstr] = v["level"]
        return  first_str
    def merge_two_str(self,str1,str2):
    
        mid_str=""
        if len(str1)!=len(str2):
            mid_str=","
        if str1[0]!=str2[0]:
            return str1+","+str2
        index=[]
        flag=True
        for i in range(len(str1)):
            #print(i)
            if flag==True:
                if str1[i]==str2[i]:            
                    continue
                else:
                    index.append(i)
                    #print(index)
                    flag=False
            else:
                #print(str1[i],str2[i])
                if str1[i]!=str2[i]:
                    #print(str1[i],str2[i])
                    continue
                else:
                    index.append(i)
                    flag=True
        start=0
        index.append(len(str1))
        merge_strs=""
        for j,end in enumerate(index):
            #print(j%2)
            if j%2==0:
                merge_strs+=str1[start:end]
                start=end
            else:
                merge_strs+=str1[start:end]+mid_str+str2[start:end]
                start=end
        if start<len(str2):
            merge_strs+=str2[start:]
        #print(merge_strs)
        return merge_strs
    def merge_new_label_str(self,contents):
        contents1=[]
        flag =[]
        for i in range(len(contents)):
            if i in flag:
                continue
            first_str=contents[i]
            second_str=""
            for j in range(i+1,len(contents)):
                if len(contents[j])!=len(contents[i]) or contents[j][0]!=contents[i][0]:
                    continue
                second_str=contents[j]
                #print(first_str,second_str)
                product_str=self.merge_two_str(first_str,second_str)
                contents1.append(product_str)
                flag.append(j)
                break
            if second_str=="":
                contents1.append(first_str)
        #print(contents1)
        #二次融合
        contents2=[]
        flag =[]
        for i in range(len(contents1)):
            if i in flag:
                continue
            first_str=contents1[i]
            product_str=first_str
            second_str=""
            for j in range(i+1,len(contents1)):
                if  contents1[j][0]!=contents1[i][0]:
                    continue
                second_str=contents1[j]
                if len(first_str)<=len(second_str):
                    product_str=self.merge_two_str(first_str,second_str)
                else:
                    product_str=self.merge_two_str(second_str,first_str)
                #contents2.append(product_str)
                flag.append(j)
            
            contents2.append(product_str)
        final_str=",".join(contents2)
        #print(final_str)  
        return  final_str
    def merge_new_scene_label(self,label_dict):
        key_first_script = {'reduce_user':'上月参与了主套餐优惠减免活动', 'high_prod_user':'使用的主套餐资费每月69元',
         'low_gsm_gprs':'使用的主套餐套包含的语音流量偏少', 'gprs_used_growth':'近几个月的流量使用连续增长', 
         'gsm_used_growth':'近几个月的语音使用连续增长', 'high_gprs_low_gsm':'上月套餐使用流量剩余量偏多而语音偏少',
         'high_gsm_low_gprs':'上月套餐使用语音剩余量偏多而流量偏少', 
         'gprs_over_now_prod':'上月流量使用超出主套餐包含的流量','gsm_over_now_prod':'上月语音使用超出主套餐包含的语音'}
        contents =[]
        for key in label_dict:
            if key in key_first_script and label_dict[key]>=1:
                contents.append(key_first_script[key])
        if  len(contents)==0 :
            return ""
        final_str=","+self.merge_new_label_str(contents)
        return final_str
    def select_top_script(self,first_scripts,top):
        res_scripts = sorted(first_scripts.items(),key=lambda x: x[1],reverse=True)
        scripts =[]
        for i in range(top):
            scripts.append(res_scripts[i][0])
        return scripts
    def product_first_script(self,label_dict):
        first_script_str = ""
        #print(label_dict)
        first_script = self.merge_first_chinese(label_dict)
        first_scripts = self.merge_str(first_script)
        #print(first_script)
        mid_str=self.merge_new_scene_label(label_dict)
        #print(mid_str)
        temp = self.activates(label_dict)
        top = int(self.top_count)
        #second_script = self.product_second_script(label_dict)
        if len(temp) == 0:
            if len(first_scripts) < top:
                scripts = self.select_top_script(first_scripts,len(first_scripts))
                if len(first_scripts) <= 2:
                    first_script_str = "且".join(scripts)+mid_str + "情况," 
                else:
                    first_script_str = ",".join(scripts[:-1])+"且"+scripts[-1]+mid_str + "情况," 
                return first_script_str
            else:
                scripts = self.select_top_script(first_scripts,top)
                first_script_str = ",".join(scripts[:-1])+"且"+scripts[-1] + "情况," 
        if len(first_script) == 0 and mid_str!="":
            first_script_str += mid_str + temp
        elif len(first_script) == 0 and mid_str == "":
            first_script_str += temp[1:]
        elif len(first_script) == 1:
            scripts = self.select_top_script(first_scripts,len(first_scripts)) 
            if mid_str!="":
                first_script_str = scripts[0] +"且"+ mid_str + "情况,"+ temp 
            else:
                first_script_str = scripts[0]  + "情况,"+ temp 
        else:
            first_scripts[temp] = 1
            if mid_str!="":
                first_scripts[mid_str] = 2
            scripts = self.select_top_script(first_scripts,top)
            first_script_str = ",".join(scripts) 
        if first_script_str[0]==',':
            first_script_str=first_script_str[1:]
        return first_script_str
        first_script_str = ""
        #print(label_dict)
        first_script = self.merge_first_chinese(label_dict)
        #print(first_script)
        mid_str=self.merge_new_scene_label(label_dict)
        #print(mid_str)
        temp = self.activates(label_dict)
        if len(temp) == 0:
            if len(first_script) <= 2:
                first_script_str = "和".join(self.merge_str(first_script) )+mid_str + "情况,"
                return first_script_str
            else:
                first_script = self.select1_first_script(first_script)
                first_script_str = "和".join(self.merge_str(first_script))+ "情况,"
        if len(first_script) == 0:
            first_script_str += temp[1:]
        elif len(first_script) == 1:
            first_script_str = mid_str+",".join(self.merge_str(first_script)) + "情况,"+ temp
        else:
            first_script = self.select1_first_script(first_script)
            first_script_str = "和".join(self.merge_str(first_script)) +mid_str+ "情况,"+ temp
        if first_script_str[0]==',':
            first_script_str=first_script_str[1:]
        return first_script_str
    def select1_first_script(self,first_script):
        first_scr = []
        temp1 = []
        temp2 = []
        temp3 = []
        temp4 = []
        for v in first_script:
            if "当月" in v["time_pro"] and v["state"] != "预超套" and (v["type"] == "流量" or v["type"] == "语音流量"):
               temp1.append(v)
               continue
            if "近三个月" in v["time_pro"] and v["state"] == "均超套":
               temp2.append(v)
               continue
            if "近三个月" in v["time_pro"] and v["state"] == "超套":
               temp3.append(v)
               continue
            if "当月" in v["time_pro"]:
                temp4.append(v)          
        if len(temp1) != 0:
            return temp1
        if len(temp2) != 0:
            return temp2
        if len(temp3) != 0:
            return temp3
        if len(temp4) != 0:
            if len(temp4) ==1:
                return temp4
            if len(temp4) ==1:
                if temp4[0]["state"] == "超套":
                    return [temp4[0]]
                else:
                    return [temp4[1]]
    def merge_first_script(self):
        label_dict = self.match_label()
        flag = 0   
        for key in label_dict:
            if label_dict[key] >= 1:
                flag = 1
                break
        first_chi_str = "您好，关注到您近几个月的套餐使用情况，"
        if flag == 0:
            return label_dict,first_chi_str
        label_dic_list = sorted(label_dict.items(),key=lambda x: x[1],reverse=True)
        label_dic = {}
        #print("start merge")        
        #for i in range(self.top_count):
        for i in range(int(self.top_count)):
            label_dic[label_dic_list[i][0]] = label_dic_list[i][1]
        #print(label_dic_list)
        for key in label_dict:
            if key in label_dic:
                continue
            label_dic[key] = 0
        #print(label_dic)
        product_script = self.product_first_script(label_dic)
        if "语音" in product_script and "流量" in product_script:
            product_script+= "现在公司有个新版套餐优惠，可以帮您多申请些流量和通话分钟数；"
        elif "语音" in product_script:
            product_script+= "现在公司有个新版套餐优惠，可以帮您多申请些通话分钟数；"
        elif "流量" in product_script:
            product_script+= "现在公司有个新版套餐优惠，可以帮您多申请些流量；"

        first_script_str = "您好，关注到您" + product_script
        return label_dict,first_script_str
    def out_content(self):
        label_dict,first_chi_str = self.merge_first_script()
        #print(first_chi_str)
        '''key_v = {'cur_month_gprs_over': "当月流量已超套", 'over_activity': "活动有到期", 'two_last_month_gprs_over': "账务月减一流量超套", 
    'next_day_num': "次日有办理的活动将到期", 'pre_day_num': "上月有办理的活动已到期", 'last_month_gsm_over': "账务月语音超套", 'last_month_gprs_over': "账务月流量超套", 
    'three_month_gsm_over': "近三个月语音均超套", 'three_month_gprs_over': "近三个月流量均超套", 'current_month_num': "本月有办理的活动已到期", 'cur_month_gsm_over': "当月语音已超套", 
    'next_month_num': "次月有办理的活动将到期", 'cur_month_gsm_will_over': "当月语音预将超套", 'cur_month_gprs_will_over': "当月流量预将超套", 
    'two_last_month_gsm_over': "账务月减一语音超套",'three_last_month_gprs_over': "账务月减二流量超套", 'three_last_month_gsm_over': "账务月减二语音超套"}
        key_id = {'cur_month_gprs_over': "0001", 'over_activity': "0013", 'two_last_month_gprs_over': "0009", 
    'next_day_num': "0016", 'pre_day_num': "0014", 'last_month_gsm_over': "0008", 'last_month_gprs_over': "0007", 
    'three_month_gsm_over': "0006", 'three_month_gprs_over': "0005", 'current_month_num': "0015", 'cur_month_gsm_over': "0002", 
    'next_month_num': "0017", 'cur_month_gsm_will_over': "0004", 'cur_month_gprs_will_over': "0003", 
    'two_last_month_gsm_over': "0010",'three_last_month_gprs_over': "0011", 'three_last_month_gsm_over': "0012"}'''
        res = []
        #print(label_dict)
        #key_id,key_v,labelname=get_conf_labels_id()
        for k in label_dict:
            
            if label_dict[k] >= 1:
                #print(k,label_dict[k])
                dic_s={}
                #dic_s["labelId"] = str(key_id[k])
                #dic_s["labelName"] = str(key_v[k])
                dic_s["labelId"] = str(self.key_id[self.key_v[k]])
                dic_s["labelName"] = str(self.key_v[k])
                dic_s["labelOperator"] = ""
                dic_s["labelOperatorValue"] = ""
                dic_s["labelOrder"] = ""
                res.append(dic_s)
        res_dic = {}
        #print(str(res).decode('string_escape'))
        #print(len(res),len(label_dict))
        res_dic["user_label"] = res
        res_dic["label_rate"] = format(len(res)/float(len(label_dict)),'.4f')
        res_dic["first_script"] = first_chi_str
        res_dic["market_script"] = first_chi_str + self.productDesc
        return res_dic
    
if __name__=='__main__':
        data = { 
        "is_gprs_over" : 0,"is_gsm_over" : 0,"gprs_all" : 0,"gsm_all" : 0,
        "gprs_residua" : 10,"gsm_residua" : 10,"avg_day_gprs" : 20,"avg_day_gsm" : 20,
        "suf_current_day_count" : 20,"avg_three_month_gsm_count" : 0, "avg_three_month_gprs_count" : 3,
        "avg_three_month_gprs_fee": 0,"avg_three_month_gsm_fee":0,"tcw_gprs_fee1" :0,
        "tcw_gsm_fee1" : 0,"gprs_all1" : 0,"gprs_main_all1" : 0,"tcw_gprs1" : 0,
        "tcw_gprs_fee3":0,
        "tcw_gsm_fee3":10,
        "tcw_gprs3":0,
        "tcw_gsm3":10,
        "gsm_zj1" : 0,"tcw_gsm1": 0,"gsm_main_all1" : 0,"tcw_gprs_fee2" : 0,"tcw_gsm_fee2" : 0,
        "tcw_gprs2" : 0,"tcw_gsm2" : 0,"is_over_activity" : 0, "pre_day_num" : 0,"current_month_num" : 0,
        "next_day_num" :0,"next_month_num" : 0,"now_prod_value" :0,"now_prod_gprs" : 100,"now_prod_gsm" : 100,
        "gprs_all" : 100,"gsm_all" :100,"avg_three_month_gsm" : 0,"avg_three_month_gprs":0,
        "productDesc":"现为你推荐一款更优惠的套餐，我为你介绍一下，套餐原价69元/月，折扣后仅需59元，包含20G流量，400分钟通话"
    }
        print("start")
        '''p = process(data)
        values= p.out_content()
        print("匹配的用户标签：")
        print(str(values).decode('string_escape'))'''
        

